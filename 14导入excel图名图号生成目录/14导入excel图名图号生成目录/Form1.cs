﻿using Autodesk.AutoCAD.ApplicationServices;
using Autodesk.AutoCAD.DatabaseServices;
using Autodesk.AutoCAD.EditorInput;
using Autodesk.AutoCAD.Geometry;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Application = Autodesk.AutoCAD.ApplicationServices.Application;

namespace _14导入excel图名图号生成目录
{
    public partial class Form1 : Form
    {
        //初始化  窗口焦点切换功能
        [DllImport("user32.dll", EntryPoint = "SetFocus")]
        public static extern int SetFocus(IntPtr hWnd);
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }




        private void button2_Click(object sender, EventArgs e)
        {
            string filename = textBox1.Text;  //从textbox里头获取文件路径
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;

            
            List<string> lstAddStr1 = new List<string>();  //读取的图名结果存在这里
            List<string> lstAddStr2 = new List<string>();  //读取的图号结果存在这里

            IWorkbook workbook = null;  //新建IWorkbook对象 表格对象
            FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);  //文件流读取
            if (filename.IndexOf(".xlsx") > 0) // 2007版本
            {
                workbook = new XSSFWorkbook(fileStream);  //xlsx数据读入workbook
            }
            else if (filename.IndexOf(".xls") > 0) // 2003版本
            {
                workbook = new HSSFWorkbook(fileStream);  //xls数据读入workbook
            }
            ISheet sheet = workbook.GetSheetAt(0);  //获取第一个工作表
            IRow row;// = sheet.GetRow(0);            //新建当前工作表行行
            for (int i = 1; i < sheet.LastRowNum; i++)  //对工作表每一行
            {
                row = sheet.GetRow(i);   //row读入第i行数据
                if (row != null)
                {
                    lstAddStr1.Add(row.GetCell(0).ToString());  //获取第i行0 列
                    lstAddStr2.Add(row.GetCell(1).ToString());  //获取第i行1列
                }

            }
            fileStream.Close();
            workbook.Close();

            

            //选择完文件在切换焦点
            SetFocus(doc.Window.Handle);       
            // 切记 锁定文档
            using (DocumentLock acLckDoc = doc.LockDocument())
            {
             
                            
                for (int n = 1 ; n < 26; n++)
                {
                    
                    String blockName = "目录首页";
                    DBText serialNum = new DBText(); // 序号
                    DBText drawingNum = new DBText(); // 图号
                    DBText drawingName = new DBText(); // 图名
                    DBText nums = new DBText(); // 图纸张数
                    nums.TextString = "1"; // 图纸张数初值
                    Point3d point1 = db.GetBlockRefCoor(blockName);
                                
                    
                    serialNum.Position = new Point3d(point1.X - 175.47, point1.Y + 200.82 - 8 * n, point1.Z);  // 序号位置
                    drawingNum.Position = new Point3d(point1.X - 169.71, point1.Y + 200.82 - 8 * n, point1.Z); // 图号位置
                    drawingName.Position = new Point3d(point1.X - 126.81, point1.Y + 200.82 - 8 * n, point1.Z); // 图名位置
                    nums.Position = new Point3d(point1.X - 50.06, point1.Y + 200.82 - 8 * n, point1.Z); // 图纸张数

                    db.CreateDBText(n.ToString(), serialNum.Position, 1.9);    // 序号写入
                    db.CreateDBText(lstAddStr1[n-1], drawingNum.Position, 1.9);  // 图号写入
                    db.CreateDBText(lstAddStr2[n-1], drawingName.Position, 1.9); // 图名写入
                    db.CreateDBText(nums.TextString, nums.Position, 1.9);      // 图纸张数写入
              
                }

            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            //开始子线程调用openfiledialog  
            System.Threading.Thread s = new System.Threading.Thread
                                       (new System.Threading.ThreadStart(GetDialogToExcel));
            s.SetApartmentState(System.Threading.ApartmentState.STA); //设置线程状态
            s.Start();
        }



        public void GetDialogToExcel()
        {
            OpenFileDialog fileDialog = new OpenFileDialog();  //选择文件对话框 
            fileDialog.Multiselect = true;                     //可不可以同时选几个文件
            fileDialog.Title = "请选择包含图名图号的EXCEL文件";
            fileDialog.Filter = "Excel Office97-2003(*.xls)|*.xls|Excel Office2007及以上(*.xlsx)|*.xlsx";
            DialogResult result = fileDialog.ShowDialog();
            if (result == DialogResult.OK)
            {
                string filepath = fileDialog.FileName;//返回文件的完整路径 
                textBox1.Text = filepath;
            }
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }


        private void button3_Click(object sender, EventArgs e)
        {
            string filename = textBox1.Text;  //从textbox里头获取文件路径
            Document doc = Application.DocumentManager.MdiActiveDocument;
            Database db = doc.Database;
            Editor ed = doc.Editor;


            List<string> lstAddStr1 = new List<string>();  //读取的图名结果存在这里
            List<string> lstAddStr2 = new List<string>();  //读取的图号结果存在这里

            IWorkbook workbook = null;  //新建IWorkbook对象 表格对象
            FileStream fileStream = new FileStream(filename, FileMode.Open, FileAccess.Read);  //文件流读取
            if (filename.IndexOf(".xlsx") > 0) // 2007版本
            {
                workbook = new XSSFWorkbook(fileStream);  //xlsx数据读入workbook
            }
            else if (filename.IndexOf(".xls") > 0) // 2003版本
            {
                workbook = new HSSFWorkbook(fileStream);  //xls数据读入workbook
            }
            ISheet sheet = workbook.GetSheetAt(0);  //获取第一个工作表
            IRow row;// = sheet.GetRow(0);            //新建当前工作表行行
            for (int i = 1; i < sheet.LastRowNum; i++)  //对工作表每一行
            {
                row = sheet.GetRow(i);   //row读入第i行数据
                if (row != null)
                {
                    lstAddStr1.Add(row.GetCell(0).ToString());  //获取第i行0 列
                    lstAddStr2.Add(row.GetCell(1).ToString());  //获取第i行1列
                }

            }
            fileStream.Close();
            workbook.Close();
            //选择完文件切换焦点
            SetFocus(doc.Window.Handle);
            // 锁定文档
            using (DocumentLock acLckDoc = doc.LockDocument())
            {               
                String blockName = "目录续页";
                DBText serialNum = new DBText(); // 序号
                DBText drawingNum = new DBText(); // 图号
                DBText drawingName = new DBText(); // 图名
                DBText nums = new DBText(); // 图纸张数
                nums.TextString = "1"; // 图纸张数初值
                List<Point3d> points = db.GetAllBlockCoors(blockName);
                //int k = 0;
                for(int m = 0; m < points.Count; m++)
                {
                    if (m < points.Count - 1)
                    {
                        for (int n = 25 + m * 28; n < 25 + (m + 1) * 28; n++)
                        {
                            serialNum.Position = new Point3d(points[m].X - 181.53, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z);  // 序号位置
                            drawingNum.Position = new Point3d(points[m].X - 174.16, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图号位置
                            drawingName.Position = new Point3d(points[m].X - 138.52, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图名位置
                            nums.Position = new Point3d(points[m].X - 55.25, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图纸张数                     
                            db.CreateDBText((n + 1).ToString(), serialNum.Position, 1.9);    // 序号写入
                            db.CreateDBText(lstAddStr1[n - 1], drawingNum.Position, 1.9);  // 图号写入
                            db.CreateDBText(lstAddStr2[n - 1], drawingName.Position, 1.9); // 图名写入
                            db.CreateDBText(nums.TextString, nums.Position, 1.9);      // 图纸张数写入
                            //k = k + 1;

                        }
                    }
                    else
                    {
                        for (int n = 25 + m * 28; n < lstAddStr1.Count + 1; n++)
                        {
                            serialNum.Position = new Point3d(points[m].X - 181.53, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z);  // 序号位置
                            drawingNum.Position = new Point3d(points[m].X - 174.16, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图号位置
                            drawingName.Position = new Point3d(points[m].X - 138.52, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图名位置
                            nums.Position = new Point3d(points[m].X - 55.25, points[m].Y + 421.93 - 8 * (n - m * 28), points[m].Z); // 图纸张数                     
                            db.CreateDBText((n + 1).ToString(), serialNum.Position, 1.9);    // 序号写入
                            db.CreateDBText(lstAddStr1[n-1], drawingNum.Position, 1.9);  // 图号写入
                            db.CreateDBText(lstAddStr2[n-1], drawingName.Position, 1.9); // 图名写入
                            db.CreateDBText(nums.TextString, nums.Position, 1.9);      // 图纸张数写入
                            //k = k + 1;
                        }
                      
                    }

                }        
            }
        }
    }
}
